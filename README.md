# Grauwoelfchen's Forge

~~https://grauwoelfchen.at/~~  
https://forge.grauwoelfchen.net/

> I used to call this project "Canvas" :-D


## Setup

```zsh
# use stable
% rustc --version
rustc 1.84.0 (9fc6b4312 2025-01-07)

# install nib-cli and nib-server
% make setup
```

See [nib](https://crates.io/crates/nib) and [nib-cli](
https://crates.io/crates/nib-cli) for details.


## Serve

```zsh
% make serve
```


## Build

```zsh
% make build
```


## License

```txt
Grauwoelfchen's Forge
Copyright (c) 2017-2025 Yasha
```

### Software

`MIT`

This is free software:  
You can redistribute it and/or modify it under the terms of
the [MIT](https://opensource.org/licenses/MIT) License.

### Posts and Images

`CC-BY-NC-SA-4.0`

Illustrations, photos and articles are licensed under the
Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International
License.

[![Creative Commons License](
https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](
http://creativecommons.org/licenses/by-nc-sa/4.0/)
